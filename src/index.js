
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';

import { createStore, applyMiddleware } from 'redux';

import reducer from './reducer';

import TableContainer from './TableContainer';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.scss';

// Configure store with thunk middleware to allow async requests
const store = createStore(
    reducer,
    composeWithDevTools(
        applyMiddleware(
            thunk
        )
    ),
);


ReactDOM.render(
    <Provider store={store}>
        <TableContainer />
    </Provider>,
    document.getElementById('root')
);

