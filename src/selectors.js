import filter from 'lodash/filter';
import find from 'lodash/find';
import moment from 'moment';

// eslint-disable-next-line
import tz from 'moment-timezone';


export const getShiftsWithTimezoneAndRole = (state) => {
    const { shifts, timezone, roles, staff } = state;

    if (!shifts) {
        return null;
    }

    const shiftMap = shifts.map(shift => ({
            employee_id: shift.employee_id,
            role_id: shift.role_id,
            id: shift.id,
            break_duration: shift.break_duration,
            start_time: moment(shift.start_time).tz(timezone),
            end_time: moment(shift.end_time).tz(timezone),
            role: find(roles, ['id', shift.role_id]),
            staff: filter(staff, (employee) => {
                return shift.employee_id === employee.id;
            })
        }),
    );
    return shiftMap;
};

export const getStaffWithRoster = (state) => {
    const { staff } = state;
    const shifts = getShiftsWithTimezoneAndRole(state);

    const staffMap = staff.map(employee => ({
        last_name: employee.last_name,
        first_name: employee.first_name,
        id: employee.id,
        roster: filter(shifts, (roster) => {
                return roster.employee_id === employee.id;
            })
        }),
    );
    return staffMap;
};