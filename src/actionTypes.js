export const RECEIVE_CONFIG = 'RECEIVE_CONFIG';
export const RECEIVE_STAFF  = 'RECEIVE_STAFF';
export const RECEIVE_ROLES = 'RECEIVE_ROLES';
export const RECEIVE_SHIFTS = 'RECEIVE_SHIFTS';
export const NEXT_WEEK = 'NEXT_WEEK';
export const PREV_WEEK = 'PREV_WEEK';
export const UPDATE_SHIFT = 'UPDATE_SHIFT';