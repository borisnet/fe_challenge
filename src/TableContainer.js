import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

import { getStaffWithRoster, getShiftsWithTimezoneAndRole } from './selectors';

import { Alert, Container, Row, Col, Badge, Spinner, Button } from 'reactstrap';

import * as actions from './actions';

import RosterTable from './components/table/RosterTable';
import RosterList from './components/list/RosterList';


class TableContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listView: false,
        }
        this.toggleView = this.toggleView.bind(this);
    }

    toggleView() {
        this.setState({listView: !this.state.listView});
    }

    componentDidMount() {
        const { actions } = this.props;
        actions.fetchData();
        this.nextWeek = this.nextWeek.bind(this);
        this.prevWeek = this.prevWeek.bind(this);
    }

    prevWeek() {
        this.props.actions.prevWeek();
    }

    nextWeek() {
        this.props.actions.nextWeek();
    }

    render() {

        const { loaded, staff, error, shiftMap } = this.props;

        if(!staff || loaded < 4) {
            return (
                <div className="loader">
                    <Spinner color="dark" />
                </div>
            );
        } else if(error) {
            return (
                <Alert color="danger">
                    Error: {this.props.error.message}
                </Alert>
            );
        }

        return (
            <Container fluid={true}>

                <Container fluid={true} className="text-center mar-bt-3">
                    <Row>

                        <Col>

                            <h1 className="d-inline-block">{this.props.location} </h1>
                            <Badge color="secondary">{this.props.timezone}</Badge>

                        </Col>

                    </Row>


                    <Row>
                        <Col>
                        <Button className="d-block float-right mar-r-l" color="info" size="sm" onClick={this.toggleView}>
                            Toggle View
                        </Button>
                        </Col>
                    </Row>
                </Container>
                <Container fluid={true}>
                    <Row className="">
                        <Col md="1" className="d-flex align-items-stretch no-pad">
                            <Button className="ml-auto" outline color="secondary" size="sm" onClick={this.prevWeek}>
                                &#60;&#60;
                            </Button>
                        </Col>
                        <Col md="10" className="no-pad">
                            {this.state.listView ? (
                                <RosterList staff={staff} shiftMap={shiftMap}/>
                            ) : (
                                <RosterTable staff={staff}/>
                            )}
                        </Col>
                        <Col md="1" className="d-flex align-items-stretch no-pad">
                            <Button className="" outline color="secondary" size="sm" onClick={this.nextWeek}>
                                &#62;&#62;
                            </Button>
                        </Col>
                    </Row>
                </Container>
                <br /><br /><br /><br />
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch),
    };
};

const mapStateToProps = (state, ownProps) => {
    const staff = getStaffWithRoster(state);
    const shiftMap = getShiftsWithTimezoneAndRole(state);
    return {
        ...ownProps,
        loaded: state.loaded,
        location: state.location,
        timezone: state.timezone,
        staff,
        shiftMap,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    TableContainer
);