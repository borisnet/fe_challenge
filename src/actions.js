import * as actionTypes from './actionTypes';

import * as config from './files/config.json';
import * as staff from './files/employees.json';
import * as roles from './files/roles.json';
import * as shifts from './files/shifts.json';




export const fetchData = () => (dispatch) => {
    dispatch({
        type: actionTypes.RECEIVE_CONFIG,
        config: config.default,
    });
    dispatch({
        type: actionTypes.RECEIVE_STAFF,
        staff: staff.default,
    });
    dispatch({
        type: actionTypes.RECEIVE_ROLES,
        roles: roles.default,
    });
    dispatch({
        type: actionTypes.RECEIVE_SHIFTS,
        shifts: shifts.default,
    });
}

export const nextWeek = () => (dispatch) => {
    dispatch({
        type: actionTypes.NEXT_WEEK
    });
}

export const prevWeek = () => (dispatch) => {
    dispatch({
        type: actionTypes.PREV_WEEK
    });
}

export const updateShift = (id, newStart, newEnd) => (dispatch) => {
    dispatch({
        type: actionTypes.UPDATE_SHIFT,
        id,
        newStart,
        newEnd,
    });
}