import moment from 'moment';
import * as actionTypes from './actionTypes';


const initialState = {
    loaded: 0,
    timezone:null,
    location: null,
    startDate: '2018-06-18T00:00:00+00:00',
    endDate: '2018-06-24T23:59:59+00:00',
    staff:[],
    roles:[],
    shifts:[],
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.RECEIVE_CONFIG:
            return { ...state, timezone:action.config.timezone, location: action.config.location, loaded: state.loaded + 1 };

        case actionTypes.RECEIVE_STAFF:
            return { ...state, staff:action.staff, loaded: state.loaded + 1 };

        case actionTypes.RECEIVE_ROLES:
            return { ...state, roles:action.roles, loaded: state.loaded + 1 };

        case actionTypes.RECEIVE_SHIFTS:
            return { ...state, shifts:action.shifts, loaded: state.loaded + 1 };

        case actionTypes.UPDATE_SHIFT:
            return { ...state, shifts: state.shifts.map(el => (el.id === action.id ? {...el, start_time: action.newStart, end_time: action.newEnd} : el)) };

        case actionTypes.NEXT_WEEK:
            return { ...state, startDate: moment(state.startDate).add(7, 'days'), endDate: moment(state.endDate).add(7, 'days'),};

        case actionTypes.PREV_WEEK:
            return { ...state, startDate: moment(state.startDate).subtract(7, 'days'), endDate: moment(state.endDate).subtract(7, 'days'),};

        default:
            return state;
    }
};

export default reducer;
