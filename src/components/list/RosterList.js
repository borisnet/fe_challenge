import React, { Component } from 'react';
import moment from 'moment';

import * as actions from '../../actions';

import EditModal from '../shift/EditModal';

// eslint-disable-next-line
import tz from 'moment-timezone';

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import {  Table } from 'reactstrap';

class RosterList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            selectedShift: null,
        }
        this.toggleEdit = this.toggleEdit.bind(this);
        this.editShift = this.editShift.bind(this);
    }

    toggleEdit() {
        this.setState({edit: !this.state.edit, selectedShift: null});
    }

    editShift(selectedShift) {
        this.setState({selectedShift: selectedShift, edit: true});
    }

    render() {
        const { startDate, endDate, staff, shiftMap } = this.props;

        const getBeginShifts = (m, shiftMap) => {
            const day = m.clone().hour(23).minute(59);
            return shiftMap.filter(shift => (day.isBetween(shift.start_time, shift.end_time)));
        }

        const getEndShifts = (m, shiftMap) => {
            const day = moment.utc(m.clone()).tz(this.props.timezone);
            return shiftMap.filter(shift => (day.isBetween(shift.start_time, shift.end_time)));
        }

        const getMorningShifts = (m, shiftMap) => {
            return shiftMap.filter(shift => {
                if(m.isSame(shift.start_time, 'day') && m.isSame(shift.end_time, 'day')) {
                    let noonCopy = shift.start_time.clone();
                    return shift.start_time.isBefore(noonCopy.hour(12).minute(0))
                }
                return false;
            });
        }

        const getAfternoonShifts = (m, shiftMap) => {
            return shiftMap.filter(shift => {
                if(m.isSame(shift.start_time, 'day') && m.isSame(shift.end_time, 'day')) {
                    let noonCopy = shift.start_time.clone();
                    return shift.start_time.isAfter(noonCopy.hour(12).minute(0))
                }
                return false;
            });
        }

        const renderBeginCell = (m, shiftUnit) => {
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className="shiftCell d-block"
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <span className="">{shiftUnit.staff[0].first_name} {shiftUnit.staff[0].last_name}</span> <br />
                    <span className="small">{shiftUnit.role.name}</span> <br />
                    Start at {shiftUnit.start_time.format('HH:mm')}
                </div>
            );
        }

        const renderEndCell = (m, shiftUnit) => {
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className="shiftCell d-block"
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <span className="">{shiftUnit.staff[0].first_name} {shiftUnit.staff[0].last_name}</span> <br />
                    <span className="small">{shiftUnit.role.name}</span> <br />
                    Finish at {shiftUnit.end_time.format('HH:mm')}
                </div>
            );
        }

        const renderWholeCell = (m, shiftUnit) => {
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className="shiftCell d-block"
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <span className="">{shiftUnit.staff[0].first_name} {shiftUnit.staff[0].last_name}</span> <br />
                    <span className="small">{shiftUnit.role.name}</span> <br />
                    {shiftUnit.start_time.format('HH:mm')} - {shiftUnit.end_time.format('HH:mm')}
                </div>
            );
        }

        const renderEndCells = (day, shiftMap) => {
            let hD = [];
            shiftMap.forEach(function(shift) {
                hD.push(renderEndCell(day, shift));
            });
            return hD;
        }

        const renderBeginCells = (day, shiftMap) => {
            let hD = [];
            shiftMap.forEach(function(shift) {
                hD.push(renderBeginCell(day, shift));
            });
            return hD;
        }

        const renderWholeCells = (day, shiftMap) => {
            let hD = [];
            shiftMap.forEach(function(shift) {
                hD.push(renderWholeCell(day, shift));
            });
            return hD;
        }

        const renderDay = (m, shiftMap) => {
            return (
                <tr key={m}>
                    <td>{m.format('dddd')}<br />{m.format('Do of MMMM, YYYY')}</td>
                    <td>{renderEndCells(m, getEndShifts(m, shiftMap))}</td>
                    <td>{renderWholeCells(m, getMorningShifts(m, shiftMap))}</td>
                    <td>{renderWholeCells(m, getAfternoonShifts(m, shiftMap))}</td>
                    <td>{renderBeginCells(m, getBeginShifts(m, shiftMap))}</td>
                </tr>
            );
        }

        const loopDays = (startDate, endDate, shiftMap) => {
            let sD = [];
            for (var m = startDate.clone(); m.isBefore(endDate); m.add(24, 'hours')) {
                sD.push(renderDay(m, shiftMap));
            }
            return sD;
        }

        const noTzStart = startDate.format('YYYY-MM-DDT00:00:00');
        const noTzEnd = endDate.format('YYYY-MM-DDT00:00:00');

        return (
            <div>
                <EditModal
                    modalOpen={this.state.edit}
                    modalToggle={this.toggleEdit}
                    selectedShift={this.state.selectedShift}
                    staff={staff}
                />
                <Table dark className="no-mar">
                    <tbody>
                        {loopDays(moment(noTzStart), moment(noTzEnd), shiftMap)}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch),
    };
};

const mapStateToProps = (state, ownProps) => {
    const { startDate, endDate, timezone } = state;
    return {
        ...ownProps,
        startDate: moment(startDate).tz(timezone),
        endDate: moment(endDate).tz(timezone),
        timezone,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    RosterList
);