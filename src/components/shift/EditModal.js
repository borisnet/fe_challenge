import React, { Component } from 'react';
import moment from 'moment'
import momentLocalizer from 'react-widgets-moment';
import find from 'lodash/find';
import filter from 'lodash/filter';

import 'react-datetime/css/react-datetime.css';
import Datetime from 'react-datetime';


import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Container, Alert } from 'reactstrap';

import * as actions from "../../actions";

// eslint-disable-next-line
import tz from 'moment-timezone';

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newStart: null,
            newEnd: null,
            error: null,
        }
        this.toggleOpen = this.toggleOpen.bind(this);
        this.saveShift = this.saveShift.bind(this);
        this.validateDates = this.validateDates.bind(this);
        this.updateEnd = this.updateEnd.bind(this);
        this.updateStart = this.updateStart.bind(this);
        moment.locale('en');
        momentLocalizer();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.selectedShift) {
            this.setState({
                newEnd: nextProps.selectedShift.end_time,
                newStart: nextProps.selectedShift.start_time,
            });
        }
    };

    updateStart = newStart => {
        this.setState({newStart: newStart});
    }

    updateEnd(newEnd) {
        this.setState({newEnd: newEnd});
    }

    validateDates() {
        const { selectedShift, staff } = this.props;
        const newStart = this.state.newStart;
        const newEnd = this.state.newEnd;
        if (newEnd.isBefore(newStart)){
            this.setState({error: 'Start date needs to be before end.'});
            return false;
        } else if (newEnd.diff(newStart, 'hours') > 8) {
            this.setState({error: 'This shift is far too long then it should be - 8 hours.'});
            return false;
        } else {
            const dude = find(staff, ['id', selectedShift.employee_id]);
            const b2b = filter(dude.roster, (shift) => {
                if(shift.id === selectedShift.id){
                    return false;
                } else if (newEnd.isBefore(shift.end_time) && newEnd.isAfter(shift.start_time)) {
                    return true;
                } else if (newStart.isBefore(shift.end_time) && newStart.isAfter(shift.start_time)) {
                    return true;
                } else if (newEnd.isBefore(shift.start_time) && shift.start_time.diff(newEnd, 'hours') < 4) {
                    return true;
                } else if (newStart.isAfter(shift.end_time) && newStart.diff(shift.end_time, 'hours') < 4) {
                    return true;
                }
            });
            if (b2b.length) {
                this.setState({error: 'This shift is too closer to another. Try with at least 4 hour break between the shifts.'});
                return false;
            }
        }
        return true;
    }

    toggleOpen() {
        this.setState({
            newEnd: null,
            newStart: null,
        });
        const { modalToggle } = this.props;
        modalToggle();
    }

    saveShift() {
        const { selectedShift } = this.props;
        if(this.validateDates(selectedShift)) {
            let newEnd = this.state.newEnd;
            if(newEnd === null) {
                newEnd = selectedShift.end_time;
            }
            let newStart = this.state.newStart;
            if(newStart === null) {
                newStart = selectedShift.start_time;
            }
            this.props.actions.updateShift(selectedShift.id, newStart, newEnd);
            this.toggleOpen();
        }
    }



    render() {
        const { modalOpen, modalToggle, selectedShift } = this.props;

        const closeBtn = <button className="close" onClick={modalToggle}>&times;</button>;

        if(!selectedShift) {
            return null;
        }

        const timeConstraints = {
            minutes: {
                step: 30,
            }
        };

        let newEnd = this.state.newEnd;
        if(newEnd === null) {
            newEnd = selectedShift.end_time;
        }
        let newStart = this.state.newStart;
        if(newStart === null) {
            newStart = selectedShift.start_time;
        }

        return (
            <Modal isOpen={modalOpen} toggle={this.toggleOpen} className="">
                <ModalHeader toggle={this.toggleOpen} close={closeBtn}>Edit Shift ID:{selectedShift.id}</ModalHeader>
                <ModalBody>
                    {this.state.error &&
                        <Container fluid={true}>
                            <Row>
                                <Col>
                                    <Alert color="danger">{this.state.error}</Alert>
                                </Col>
                            </Row>
                        </Container>
                    }
                    <Container fluid={true}>
                        <Row>
                            <Col md="5">
                                Start <Datetime
                                    name="newStart"
                                    dateFormat="DD MMM YYYY"
                                    timeFormat="HH:mm"
                                    value={this.state.newStart}
                                    onChange={this.updateStart}
                                    timeConstraints={timeConstraints}
                                />
                            </Col>
                            <Col md="1">
                                <br />_
                            </Col>
                            <Col md="5">
                                End <Datetime
                                    name="newEnd"
                                    dateFormat="DD MMM YYYY"
                                    timeFormat="HH:mm"
                                    value={this.state.newEnd}
                                    onChange={this.updateEnd}
                                    timeConstraints={timeConstraints}
                                />
                            </Col>
                        </Row>
                    </Container>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.saveShift}>Submit</Button>{' '}
                    <Button color="secondary" onClick={this.toggleOpen}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch),
    };
};

const mapStateToProps = (state, ownProps) => {
    const { timezone } = state;
    return {
        ...ownProps,
        timezone
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    EditModal
);