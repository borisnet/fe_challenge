import React, { Component } from 'react';
import moment from 'moment';
import { Table } from 'reactstrap';

import * as actions from '../../actions';

import EditModal from '../shift/EditModal';

// eslint-disable-next-line
import tz from 'moment-timezone';

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

class RosterTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            selectedShift: null,
        }
        this.toggleEdit = this.toggleEdit.bind(this);
        this.editShift = this.editShift.bind(this);
    }

    toggleEdit() {
        this.setState({edit: !this.state.edit, selectedShift: null});
    }

    editShift(selectedShift) {
        this.setState({selectedShift: selectedShift, edit: true});
    }

    render() {
        const { startDate, endDate, staff } = this.props;

        const renderDay = (m) => {
            return (<th className="text-center" key={m}>{m.format('dddd')}<br />{m.format('Do of MMMM, YYYY')}</th>);
        }

        const headerDays = (startDate, endDate) => {
            let hD = [];
            for (var m = moment(startDate); m.isBefore(endDate); m.add(1, 'days')) {
                hD.push(renderDay(m));
            }
            return hD;
        }

        const checkShift = (m, roster) => {
            return roster.filter(shift => (m.isBetween(shift.start_time, shift.end_time)));
        }

        const renderBeginCell = (m, shiftUnit) => {
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className="float-right shiftCell beginCell"
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <span className="small">{shiftUnit.role.name}</span> <br />
                    {shiftUnit.start_time.format('HH:mm')}<span className="float-right">&#160;&#160;-&#160;</span>
                </div>
            );
        }

        const renderEndCell = (m, shiftUnit) => {
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className="float-left shiftCell endCell"
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <br />
                    {shiftUnit.end_time.format('HH:mm')}
                </div>
            );
        }

        const renderWholeCell = (m, shiftUnit) => {
            let noonCopy = shiftUnit.start_time.clone(); let classes = 'shiftCell float-left';
            if(shiftUnit.start_time.isAfter(noonCopy.hour(12).minute(0))) {
                classes = 'shiftCell';
            }
            return (
                <div
                    onClick={() => this.editShift(shiftUnit)}
                    className={classes}
                    key={shiftUnit.id}
                    style={{ color: shiftUnit.role.text_colour, background: shiftUnit.role.background_colour}}
                >
                    <span className="small">{shiftUnit.role.name}</span> <br />
                    {shiftUnit.start_time.format('HH:mm')} - {shiftUnit.end_time.format('HH:mm')}
                </div>
            );
        }

        const renderShift = (day, employee) => {
            let hD = [];
            const startOfDay = day.startOf('day');
            const endOfDay = startOfDay.clone().endOf('day');
            let roster = employee.roster;
            for (var m = startOfDay; m.isBefore(endOfDay); m.add(30, 'm')) {
                const shiftUnit = checkShift(m, roster);
                if(shiftUnit.length) {
                    const matchedUnit = shiftUnit[0];
                    if (matchedUnit.start_time.isSame(matchedUnit.end_time, 'day')) {
                        hD.push(renderWholeCell(m, matchedUnit));
                    } else if (matchedUnit.start_time.isSame(m, 'day')) {
                        hD.push(renderBeginCell(m, matchedUnit));
                    } else {
                        hD.push(renderEndCell(m, matchedUnit));
                    }
                    roster = roster.filter(shift => (shift.id !== matchedUnit.id));
                }
            }
            return hD;
        }


        const staffDays = (startDate, endDate, employee) => {
            let sD = [];
            for (var m = startDate.clone(); m.isBefore(endDate); m.add(23, 'hours')) {
                sD.push(renderShift(m, employee));
            }
            return sD;
        }

        const renderStaff = (startDate, endDate, staff) => {
            return staff.map((employee) => {
                const { id, first_name, last_name } = employee;
                const days = staffDays(this.props.startDate, this.props.endDate, employee);
                return (
                    <tr key={id}>
                        <td className="col-orange align-middle">{first_name} {last_name}</td>
                        {days.map((day, index) => <td className="no-pad-sides text-center" key={index}>{day}</td>)}
                    </tr>
                );
            });
        }

        return (
            <div>
                <EditModal
                    modalOpen={this.state.edit}
                    modalToggle={this.toggleEdit}
                    selectedShift={this.state.selectedShift}
                    staff={staff}
                />
                <Table dark className="no-mar">
                    <thead>
                        <tr>
                            <th></th>
                            {headerDays(startDate, endDate)}
                        </tr>
                    </thead>
                    <tbody>
                        {renderStaff(startDate, endDate, staff)}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch),
    };
};

const mapStateToProps = (state, ownProps) => {
    const { startDate, endDate, timezone } = state;
    return {
        ...ownProps,
        startDate: moment(startDate).tz(timezone),
        endDate: moment(endDate).tz(timezone),
        timezone,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    RosterTable
);