This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Quick Set Up

In the project directory, run:

### `yarn && yarn start`

Runs the app in the development mode, Enjoy.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Use large arrow buttons on left and run of the table to scroll weeks.

Click toggle view to switch between staff roster and daily visual list.

Click into any of the displayed shifts for editing.

## Assumptions

1. This is a coding challenge to test candidates React skills and flow of fought when solving problems.
Completing over the weekend I had to make choices and juggle priorities to make this happen, while still 
fitting  a quick surfing session in Sunday afternoon.

2. If this was destined for production release I would add a lot more form validation then there is now. 
However, that said, there is a good starting set of validation tests in the shift editing function already.

3. When implementing `editing` of shift dates I did not go as far as store data 
anywhere other then the Redux Store. In real world this would have been sent to the server via API call.

4. Rostering data set is a little bit unusual - real world scenario would have had a recurring set of shifts
and a solution managing updates / deviations from the recurring set. Hoping the test set provided with dates 
was done so for simplicity and not for me to assume this was supposed to be recurring.

5. Start and End dates for initial page load of week calendar view are hardcoded to match that in the 
test json provided - again for simplicity this was the easiest. Real world would have defaulted to 
current date and had a recurring roster to display.

6. Unit Tests and automated testing would have been essential in a production build.

## Resolution

I have build rostering solution once in the past - recurring meal ordering for long term hospital patients in New Zealand. 
It took a team of multiple developers over 2 months, was a very complex task, and took a long time to polish out even after the release.

So to knock off a rostering solution in React, over a weekend for this test - I knew I was in for a hell of a ride. I dug in and made it happen - everything works, all points addressed, some validation included. 

I have used `reactstrap` with `bootstrap 4` for the first time in this project - must say very impressed, everything worked and looked good out of the box. 

`Files` - moved into src directory for easier/quicker access. I use `selectors.js` to organise data from get go into better mapped arrays for quicker processing when presenting.

`Visualisation of some sort` - the only other visualisation I could think of is a differently organised table, that I called a list, 
displaying data by day and all the shifts in that day so someone looking at it can see whats going on better. I hope requirement wasn't to give you a graph of some sort - because I could have done it easily...

`Time Constraints` - having only a weekend to do this, you get to see what must be the worst code I ever written. Normally what I would have done next is continue component splitting - there are a few more levels that it can branch out. 
There is also a fare amount of code duplication that I would have refactored, functions doing similar things or rendering things in similar ways. 
Components moved to individual folders are the structure I normally follow, often I would have a `container` component plus actions, action types and reducer in that folder, 
all organised by root reducer in thew root. Didn't need in this project and one set of actions/reducer was enough for everything.

`Challenges` - Timezones obviously can be tricky at times, especially when it comes to comparing dates. Shifts that start on one day and finish in the other are also fun from filtering and presenting perspective.

## My Awesome Planning board

![Alt text](./src/files/planning.jpg?raw=true "Planning")